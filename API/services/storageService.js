const db = require('../database/dbService');
const fs = require('fs');
const shortId = require('shortid');
const mime = require('mime-types');
const path = require('path');

// Every async service in the system should return Promise for better composition in future

const getUserFiles = userId => {
  return new Promise((resolve, reject) => {
    db.storage.find({ userId: userId }, (err, user) => {
      if (err) {
        return reject(err);
      }
      if (!user) {
        return reject();
      }
      return resolve(user);
    });
  });
};

const _toFileResponse = (file) => ({ fileName: file.fileName });
const _toFileWithMetadataResponse = (file) => ({
  fileName: file.fileName,
  size: file.size,
  modified: file.modified,
  removed: file.removed
});

const getFileInfoById = (fileId, metadata) => {
  return new Promise((resolve, reject) => {
    db.storage.findOne({ fileId: fileId }, (err, file) => {
      if (err) {
        return reject(err);
      }
      console.log('HERE', file)
      if (!file) {
        return reject();
      }
      return resolve(file);
    });
  });
};

const updateFile = (fileId, fileData) => {
  return new Promise((resolve, reject) => {
    db.storage.update({ fileId: fileId }, { $set: fileData }, { returnUpdatedDocs: true }, (err, numAffected, file) => {
      if (err) {
        console.log(err);
        return reject(err);
      }
      if (!file) {
        console.log('ERROR, no file');
        return reject();
      }
      return resolve(file);
    });
  });
};
// First marks file entity in DB as removed for immidiate effect and 
// then deletes the physical file. In real life physical file removal should be async. 
const removeFile = (fileId) => {
  return updateFile(fileId, { removed: new Date() }).then(file => {
    // The best way to send a message to a queue and then special consumer can remove the file in it's turn
    file && fs.unlink('fileStorage/' + file.localFileName);
    return file;
  });
}

const toggleFilePublic = (fileId, public) => {
  return updateFile(fileId, { public: public });
}

// In real life better to use Typescript for static typing for better structuring and refactoring
const buildFileEntry = (userId, file) => {

  const fileId = shortId.generate();
  const fileExt = path.extname(file.name);
  const obj = {
    userId: userId,
    fileId: fileId,
    mime: mime.lookup(file.name),
    fileName: file.name,
    // Since file name is not unique, file renamed and saved with generated id
    localFileName: `${fileId}${fileExt}`,
    size: file.data.byteLength,
    modified: new Date(),
    public: false,
    removed: ''
  };
  return obj;
}

// In this example all the files are stored with unique id in "fileStorage" folder.
// To atchive a better scale, storing the files in specialized DB could be also an option.
// Also "cold storage" logic could be applied: We could create a service that will check weekly/daily
// files that their modified date more than x week, and it could move it to file storage that slower 
// than regular one and mark in db 'coldStorage=true' 
// When user trys to download the file, our service will pick it from the correct location
const saveFile = (userId, file) => {
  return new Promise((resolve, reject) => {
    const fileEntry = buildFileEntry(userId, file);
    file.mv('fileStorage/' + fileEntry.localFileName, (err) => {
      if (err) {
        return reject(err);
      }
      db.storage.insert([fileEntry], (error, newObj) => {
        if (error) {
          return reject(error);
        }
        return resolve(newObj);
      })
    });
  });
};

const getFileStoragePath = file => path.join(__dirname, `../../fileStorage/${file.localFileName}`);

module.exports = {
  getUserFiles: getUserFiles,
  getFileInfoById: getFileInfoById,
  saveFile: saveFile,
  getFileStoragePath: getFileStoragePath,
  updateFile: updateFile,
  removeFile: removeFile,
  toggleFilePublic: toggleFilePublic,
  _toFileResponse: _toFileResponse,
  _toFileWithMetadataResponse: _toFileWithMetadataResponse
};