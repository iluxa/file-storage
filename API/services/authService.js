const db = require('../database/dbService');

// Every async service in the system should return Promise for better composition in future

// Simplified login for the demo
const getUserByUserName = (userName) => {
  return new Promise((resolve, reject) => {
    db.users.findOne({ userName: userName }, (err, user) => {
      if (err) {
        return reject(err);
      }
      if (!user) {
        return reject();
      }
      return resolve(user);
    });
  });
};

const getUserByToken = (token) => {
  return new Promise((resolve, reject) => {
    db.users.findOne({ token: token }, (err, user) => {
      if (err) {
        return reject(err);
      }
      if (!user) {
        return reject();
      }
      return resolve(user);
    });
  });
};

const trySeedUsers = () => {
  db.users.count({}, (err, count) => {
    if (err) {
      return console.log(err);
    }
    if (count === 0) {
      console.log('Seeding users data')
      db.users.insert([
        { userName: 'User 1', token: 'qAzef32F', userId: 'usr_qAzef32F' },
        { userName: 'User 2', token: 'hT9Lmdx', userId: 'usr_hT9Lmdx' }
      ]);
    }
  });
}

module.exports = {
  getUserByUserName: getUserByUserName,
  getUserByToken: getUserByToken,
  trySeedUsers: trySeedUsers
};