// Using javascript nedb for the purpose of the demo
const Datastore = require('nedb');
const db = {
  users: new Datastore({ filename: 'db/rapid_api_users.db' }),
  storage: new Datastore({ filename: 'db/rapid_api_storage.db' })
};
db.users.loadDatabase((err) => err && console.log('Failed to init database', err));
db.storage.loadDatabase((err) => err && console.log('Failed to init database', err));

module.exports = db;