const express = require("express");
const router = express.Router();
const authService = require('../services/authService');

// Sign in is simplified for this demo
router.post('/login', (req, res) => {
  if (!req.body.userName) {
    return res.status(400).json({ error: 'Wrong Credentials' });
  }
  authService.getUserByUserName(req.body.userName).then(user => {
    res.cookie('token', user.token, { httpOnly: false, path: '/', maxAge: 90000 });
    return res.json({ user });
  }).catch(err => {
    console.log(err)
    res.status(400).json({ error: 'Wrong Credentials' });
  });
});


module.exports = router;