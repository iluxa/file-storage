const express = require("express");
const fileUpload = require('express-fileupload');
const _ = require('lodash');
const router = express.Router();
const protected = require('../middlewares/protectedMiddleware');
const storageService = require('../services/storageService');

const resourceNotFoundResponse = res => res.status(404).json({ error: 'Resource Not Found' });
const canAccessFileResource = (file, req) => (file.public || file.userId === _.get(req, 'user.userId', null));

// Returns all user files
router.get('/', protected, (req, res) => {
  storageService.getUserFiles(req.user.userId).then(files => {
    return res.json({ files, user: req.user })
  }).catch(err => {
    console.log(err)
    return resourceNotFoundResponse(res);
  });
});

// Returns file information by file id
router.get('/:fileId/:fileName', (req, res) => {
  storageService.getFileInfoById(req.params.fileId).then(file => {
    if (file.removed && !req.query.metadata) {
      // returns 404 in case of removed and not in metadata mode
      return resourceNotFoundResponse(res);
    }
    // Reurns info in case the file is public or belongs to authenticated user
    if (canAccessFileResource(file, req)) {
      return res.json(req.query.metadata ? storageService._toFileWithMetadataResponse(file) : storageService._toFileResponse(file));
    } else {
      return resourceNotFoundResponse(res);
    }
  }).catch(err => {
    console.log(err)
    return resourceNotFoundResponse(res);
  });
});

// Toggles file public
router.patch('/:fileId', protected, (req, res) => {
  storageService.toggleFilePublic(req.params.fileId, req.body.public).then(file => {
    return res.json(file);
  }).catch(err => {
    console.log(err)
    return resourceNotFoundResponse(res);
  });
});

// removes file
router.delete('/:fileId', protected, (req, res) => {
  storageService.removeFile(req.params.fileId).then(file => {
    return res.json(file);
  }).catch(err => {
    console.log(err)
    return resourceNotFoundResponse(res);
  });
});

router.get('/download/:fileId/:fileName', (req, res) => {
  storageService.getFileInfoById(req.params.fileId, req.query.metadata).then(file => {

    // 404 if removed
    if (file.removed) {
      return resourceNotFoundResponse(res);
    }
    // Downloads in case the file is public or belongs to authenticated user
    if (canAccessFileResource(file, req)) {
      // res.download handles correct download response including the mime type
      return res.download(storageService.getFileStoragePath(file));
    } else {
      return resourceNotFoundResponse(res);
    }
  }).catch(err => {
    console.log('Error', err)
    return resourceNotFoundResponse(res);
  });
});


// Uploads file
router.post('/', protected, fileUpload(), (req, res) => {
  if (!req.files) {
    return res.status(400).send('No files were uploaded.');
  }
  storageService.saveFile(req.user.userId, req.files.file).then(newFile => {
    return res.json({ newFile });
  }).catch(err => {
    return res.status(400).send('No files were uploaded.');
  });
});


module.exports = router;