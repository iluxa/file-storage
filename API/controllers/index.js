const express = require("express");
const router = express.Router();
const authController = require('./authController');
const fileStorageController = require('./fileStorageController');
const authMiddleware = require('../middlewares/authMiddleware');


// API entry point, here new api version could be pluged 
router.use('/v1/auth', authController);
router.use('/v1/storage', authMiddleware, fileStorageController);

module.exports = router;