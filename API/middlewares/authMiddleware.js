const authService = require('../services/authService');


// Auth middleware detects token in header or cookie
// the only responsibility of this middleware is to authenticate and attach user object to request and pass it foreward
// In real life scenario it should be Redis or MemCached for better performance
module.exports = (req, res, next) => {
  const token = req.headers['x-auth'] || req.cookies.token;
  if (!token) {
    return next();
  }
  authService.getUserByToken(token).then(user => {
    if (!user) {
      return next();
    }
    req.user = user;
    return next();
  }).catch(err => {
    return next();
  })
};