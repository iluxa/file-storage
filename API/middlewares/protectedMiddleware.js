

// Accepts only authenticated users - request that has the user object
// Should be palced/chained after the authMiddleware
module.exports = (req, res, next) => {
  if (!req.user) {
    return res.status(403).json({ error: 'Access Forbidden' });
  }
  return next();
}