const express = require("express");
const app = express();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
var path = require('path');
const authService = require('./API/services/authService');
const api = require('./API/controllers');

// Bootstraping express
app.use(cors());
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use("/api", api);
app.listen(process.env.PORT || 9000, () => console.log("API server is running on port 9000"));
authService.trySeedUsers();