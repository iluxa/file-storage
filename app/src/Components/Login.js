import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import '../App.css';
import * as api from '../services/api';


export default props => {

  const handleLoginClick = (userName) => {
    api.login(userName).then(user => {
      props.onLogin(user);
    });
  }
  return (
    <div className="login">
      <Paper style={{ padding: '20px' }} zDepth={1}>
        <h4>Login</h4>
        <div className="flex">
          <RaisedButton primary={true} label="Login as User 1" onClick={() => handleLoginClick('User 1')} />
          <RaisedButton primary={true} label="Login as User 2" onClick={() => handleLoginClick('User 2')} />
        </div>
      </Paper>
    </div>
  );
}
