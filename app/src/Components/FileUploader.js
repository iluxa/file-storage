import React from 'react'
import axios, { post } from 'axios';
import RaisedButton from 'material-ui/RaisedButton';

class FileUploader extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      file: null
    }
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
    this.fileUpload = this.fileUpload.bind(this)
  }
  onFormSubmit(e) {
    e.preventDefault() // Stop form submit
    this.fileUpload(this.state.file).then((response) => {
      this.setState({ file: null });
      this.props.onDone && this.props.onDone(response.data);
    })
  }
  onChange(e) {
    this.setState({ file: e.target.files[0] })
  }
  fileUpload(file) {
    const formData = new FormData();
    formData.append('file', file)
    const config = {
      withCredentials: true,
      headers: {
        'content-type': 'multipart/form-data'
      }
    }
    return post(this.props.uploadUrl, formData, config)
  }

  render() {
    return (
      <form onSubmit={this.onFormSubmit}>
        <input type="file" id="fileToUpload" name="fileToUpload" onChange={this.onChange} />
        {this.state.file && <RaisedButton label="Upload" type="submit" primary={true} />}
      </form>
    )
  }
}



export default FileUploader