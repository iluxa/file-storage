import axios from 'axios';


const toUrl = path => `http://localhost:3000${path}`;

function deleteAllCookies() {
  var cookies = document.cookie.split(";");

  for (var i = 0; i < cookies.length; i++) {
      var cookie = cookies[i];
      var eqPos = cookie.indexOf("=");
      var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
      document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
  }
}

export const login = (userName) => {
  deleteAllCookies();
  return axios(toUrl('/api/v1/auth/login'), {
    method: 'post',
    data: { userName },
    withCredentials: true,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    }
  }).then(res => {
    console.log(res);
    // document.cookie = 'token=' + res.data.user.token + ';';
    return res.data.user;
  });
}

export const getUserFiles = () => axios(toUrl('/api/v1/storage')).then(res => {
  console.log(res.data.files)
  return res.data.files;
});

export const togglePublicFile = (fileId, isPublic) => axios(toUrl(`/api/v1/storage/${fileId}`), {
  method: 'patch',
  data: { public: isPublic },
  withCredentials: true,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  }
}).then(res => {
  console.log(res.data.files)
  return res.data;
});

export const deleteFile = (fileId) => axios(toUrl(`/api/v1/storage/${fileId}`), {
  method: 'delete',
  withCredentials: true,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  }
}).then(res => {
  console.log(res.data.files)
  return res.data;
});