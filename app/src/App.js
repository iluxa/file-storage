import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import Toggle from 'material-ui/Toggle';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import Dialog from 'material-ui/Dialog';
import './App.css';
import { getUserFiles, togglePublicFile, deleteFile } from './services/api';
import FileUploader from './Components/FileUploader';
import Login from './Components/Login';


class App extends Component {

  state = { user: null, files: [], showRemoved: true }

  hanleLogin = (user) => {
    this.setState({ user });
    getUserFiles().then(files => this.setState({ files }))
  }

  togglePublic = (fileId, checked) => {
    console.log(fileId, checked);
    togglePublicFile(fileId, checked).then(res => {
      getUserFiles().then(files => this.setState({ files }))
    });
  }
  handleFileDelete = fileId => {
    deleteFile(fileId).then(res => {
      getUserFiles().then(files => this.setState({ files }))
    })
  }
  handleUploadDone = () => {
    getUserFiles().then(files => this.setState({ files }))
  }

  renderFilesList = () => {
    const files = this.state.files.filter(f => !f.removed);
    const removedFiles = this.state.files.filter(f => !!f.removed);

    const rows = (this.state.showRemoved ? this.state.files : files).map(file => {
      return (
        <TableRow ke={file.fileId} style={file.removed ? { color: 'red', opacity: '0.5' } : {}}>
          <TableRowColumn>{file.fileId}</TableRowColumn>
          <TableRowColumn>{file.fileName}</TableRowColumn>
          <TableRowColumn>{file.mime}</TableRowColumn>
          <TableRowColumn>{file.size || 0} Bytes</TableRowColumn>
          <TableRowColumn>
            <FlatButton label="Show..." primary={true} onClick={() => this.setState({ popover: file })} />
          </TableRowColumn>
          <TableRowColumn>
            <Toggle
              label="Shared"
              toggled={file.public}
              onToggle={(_, checked) => this.togglePublic(file.fileId, checked)}
            />
          </TableRowColumn>
          <TableHeaderColumn>{file.removed ?
            <span className="small">{file.removed}</span>
            :
            <FlatButton label="Remove" secondary={true} onClick={() => this.handleFileDelete(file.fileId)} />
          }

          </TableHeaderColumn>
        </TableRow>
      );
    });

    return (
      <div>
        <div className="flex space">
          <h3>
            <span> Files {files.length}, removed {removedFiles.length}</span>
          </h3>
          <FileUploader uploadUrl="http://localhost:3000/api/v1/storage" onDone={this.handleUploadDone} />
        </div>
        <Table>
          <TableHeader displaySelectAll={false}>
            <TableRow>
              <TableHeaderColumn>ID</TableHeaderColumn>
              <TableHeaderColumn>Name</TableHeaderColumn>
              <TableHeaderColumn>mime type</TableHeaderColumn>
              <TableHeaderColumn>Size</TableHeaderColumn>
              <TableHeaderColumn>Download URL</TableHeaderColumn>
              <TableHeaderColumn>Shared</TableHeaderColumn>
              <TableHeaderColumn>
                <Toggle
                  label="Removed"
                  toggled={this.state.showRemoved}
                  onToggle={(_, showRemoved) => this.setState({ showRemoved })}
                />
              </TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {rows}
          </TableBody>
        </Table>
        <Dialog
          title={`URL's for "${this.state.popover && this.state.popover.fileName}"`}
          actions={[<FlatButton
            label="close"
            primary={true}
            keyboardFocused={true}
            onClick={() => this.setState({ popover: null })}
          />]}
          modal={false}
          open={this.state.popover}
          onRequestClose={this.handleClose}
        >
          {this.state.popover && <div>
            <span>{`Download url`} {this.state.popover.public ? '[PUBLIC] - ' : '[PRIVATE] - '}  </span>
            <a href={`http://localhost:3000/api/v1/storage/download/${this.state.popover.fileId}/${this.state.popover.fileName}`}>
              {`http://localhost:3000/api/v1/storage/download/${this.state.popover.fileId}/${this.state.popover.fileName}`}
            </a>
            <br />
            <br />
            <span>File Info  </span>
            <a target="_blank" href={`http://localhost:3000/api/v1/storage/${this.state.popover.fileId}/${this.state.popover.fileName}`}>
              {`http://localhost:3000/api/v1/storage/${this.state.popover.fileId}/${this.state.popover.fileName}`}
            </a>
            <br />
            <br />
            <span>File Info with metadata  </span>
            <a target="_blank" href={`http://localhost:3000/api/v1/storage/${this.state.popover.fileId}/${this.state.popover.fileName}?metadata=true`}>
              {`http://localhost:3000/api/v1/storage/${this.state.popover.fileId}/${this.state.popover.fileName}?metadata=true`}
            </a>
          </div>}
        </Dialog>
      </div>
    );
  }
  renderContent() {
    if (!this.state.user) {
      return <Login onLogin={this.hanleLogin} />;
    }
    return <div>
      <AppBar
        title={`Rapid API file Storage. Hi ${this.state.user.userName}`}
        onTitleClick={() => true}
        iconElementLeft={<div />}
        iconElementRight={<div />}
      />
      {this.renderFilesList()}
    </div>;
  }
  render() {
    return (
      <MuiThemeProvider>
        <div className="App">
          <div className="container">
            {this.renderContent()}
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
