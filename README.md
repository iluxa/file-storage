# File Storage API and client


## Usage

### Server API
`npm install`

`npm start`

*NOTE! Security is simplified for the demo.*

#### Endpoints:

`POST /api/v1/auth/login` -  request `{body: 'user name'}` returns `{token: '213', userName: 'User 1'}` etc...

`GET /api/v1/storage`  returns array of files

`GET /api/v1/storage/:fileId/:fileName`  returns a file information - 404 if file deleted

`GET /api/v1/storage/:fileId/:fileName?metadata=true`  returns extended file information event if file is deleted

`PATCH /api/v1/storage/:fileId` request `{public: boolean}` returns `Updated file`. Method to toggle

`DELETE /api/v1/storage/:fileId` - First marks file entity in DB as removed for immidiate effect and then deletes the physical file. In real life physical file removal should be async. THe best way to send a message to a queue and then special consumer can remove the file in it's turn

API Will run on port `9000`

### Client App

*Simple react based client that allows simple login for two predefined users `"User 1"` and `"User 2"`.*
*It allows file removal, public toggle and by clicking on `"SHOW..."` button, popup will appear with all the nessesary links*
*Just for the purpose of the demo I show the removed files, but it can be filtered out by "Removed" toggle in the table header*

`cd app/`

`yarn install` or `npm install`

`npm start` - It will run a fake server on port `3000` that will proxy all the `/api/*` calls to `http://locahost:9000/api/`.


